package FBTestCases;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class FBTests {

    WebDriver driver;

    @BeforeMethod
    void logintoFBforFurtherTesting() throws MalformedURLException {
        driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), DesiredCapabilities.chrome());

        driver.get("https://www.facebook.com");
//        driver.manage().window().fullscreen();
        WebElement emailField = driver.findElement(By.id("email"));
        emailField.clear();
        emailField.sendKeys("litstest@i.ua");
        driver.findElement(By.id("email"));

        WebElement passField = driver.findElement(By.id("pass"));
        passField.clear();
        passField.sendKeys("Linux-32");

        WebElement loginButton = driver.findElement(By.id("loginbutton"));
        loginButton.click();


        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.findElement(By.tagName("body")).sendKeys(Keys.ESCAPE);



    }

    @Test
    void findFriendsTestCase() throws Exception

    {
        //Looking for Find Friends button
        WebElement findFriendsButton = driver.findElement(By.id("findFriendsNav"));
        findFriendsButton.click();

        //Adding Friend
//        WebElement clickOnSecondAddFriendButton = driver.findElement(By.xpath(".//*[@id='fbSearchResultsBox']//li[2]//button[1]"));
//        clickOnSecondAddFriendButton.click();
        List <WebElement> links = driver.findElements(By.id("fbSearchResultsBox"));
        for (int i = 0; i < links.size(); i++) {
            links = driver.findElements(By.className("FriendButton"));
            links.get(2).click();
        }



    }

    @Test
    public void logoutFromFBTestCase() throws Exception{
        //Finding and clicking on User Nav Dropdown
        WebElement userOptionsDropdown = driver.findElement(By.id("userNavigationLabel"));
        userOptionsDropdown.click();

        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

        //Click on LogOut button
        driver.findElement(By.xpath(".//li[contains(@data-gt,'menu_logout')]")).click();


        // wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("reg_box")));

    }

}