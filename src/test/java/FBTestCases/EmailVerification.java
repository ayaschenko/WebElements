package FBTestCases;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class EmailVerification {

    WebDriver driver;


    @BeforeMethod

    void beforeMethod() throws MalformedURLException {
        driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), DesiredCapabilities.chrome());
        driver.get("https://www.facebook.com/");

    }

    @DataProvider(name = "testEmails")
    public Object[][] getData() throws Exception {
        Object[][] data = new Object[3][1];
        data [0][0] = "not email@@";
        data [1][0] = "invalid email@";
        data [2][0] = "ayaschenko@@@";


        return data;



    }
    @Test(dataProvider = "testEmails")
    void   emailVerificationTestCase(String invalidEmails) throws Exception{
        driver.get("https://www.facebook.com/");
        WebElement emailField = driver.findElement(By.xpath(".//*[@id='reg_form_box']//*[@name = 'reg_email__']"));
        emailField.clear();
        emailField.sendKeys(invalidEmails);

        WebElement passField = driver.findElement(By.id("password_field"));
        passField.click();

        WebElement alertIcon = driver.findElement(By.xpath(".//*[@id='reg_form_box']/div[2]/div[1]/i[1]"));

        Wait<WebDriver> wait = new FluentWait<WebDriver>(driver).withMessage("Element was not found").withTimeout(2, TimeUnit.SECONDS).pollingEvery(1, TimeUnit.SECONDS);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath (".//*[@id='reg_form_box']/div[2]/div[1]/i[1]")));


    }

}

