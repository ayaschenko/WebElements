package FBTestCases;

import org.apache.commons.exec.ExecuteWatchdog;
import org.openqa.selenium.*;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import Waiters.TMWaiters;

import java.lang.reflect.Array;
import java.net.URL;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import PageElements.TMpageElements;

public class TMtest {
    WebDriver driver;
    WebDriverWait wait;
    WebElement cookie;
    TMpageElements language;

    @BeforeMethod
    void firstOfAll() throws Exception{

        driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), DesiredCapabilities.chrome());
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        TMpageElements language = new TMpageElements(driver);
        language.openURL("https://www.templatemonster.com");
    }

    @Test
    void checkCookie()throws Exception{
        TMWaiters wait = new TMWaiters(driver);
        wait.waitForCookie();

        TMpageElements language = new TMpageElements(driver);
        language.findValueOfThisCookieByName("aff");

        wait.waitForCheckingIfCookieValueIsCorrect("aff","TM");

//        Assert.assertEquals(language.findValueOfThisCookieByName("aff"), "TM");
    }

    @DataProvider(name = "languageList")
    public Object[][] getData() throws Exception {
        Object[][] data = new Object[3][1];
        data[0][0] = "CN";
        data[1][0] = "UA";
        data[2][0] = "ES";

        return data;
    }

    @Test(dataProvider = "languageList")
    void switchLanguage(String langPicker)throws Exception{
        TMpageElements language = new TMpageElements(driver);
        language.clickOnLanguageDropdown();
        driver.findElement(By.xpath(".//*[@id='menu-"+ langPicker + "-locale']")).click();
        TMWaiters wait = new TMWaiters(driver);
        wait.waitForNeededLangInURL(langPicker.toLowerCase());
    }
}