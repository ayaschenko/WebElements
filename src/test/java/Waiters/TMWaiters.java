package Waiters;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import FBTestCases.TMtest;

public class TMWaiters {
    WebDriver driver;

    public TMWaiters(WebDriver driver) {
        this.driver = driver;
    }


    public void waitForCookie() {
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until((WebDriver webdriver) -> webdriver.manage().getCookieNamed("aff") != null);
    }

    public void waitForNeededLangInURL(String whatShouldBeInURL) {
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until((WebDriver webdriver) -> webdriver.getCurrentUrl().contains(whatShouldBeInURL));
    }

    public void waitForCheckingIfCookieValueIsCorrect(String cookieValue, String cookieName) {
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until((WebDriver webdriver) -> webdriver.manage().getCookieNamed(cookieValue).getValue() == cookieName);

    }

}