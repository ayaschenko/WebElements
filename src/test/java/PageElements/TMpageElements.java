package PageElements;

import org.eclipse.jetty.util.thread.strategy.ExecuteProduceConsume;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.DataProvider;

import java.net.URL;
import java.rmi.Remote;

public class TMpageElements {
    WebDriver driver;


    public TMpageElements (WebDriver driver) throws Exception{
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = ".//*[@id='header']/div[2]/ul/li[1]/span")
    WebElement langDropdown;


//    @FindBy(xpath = ".//*[@id='menu-"+ langPicker + "-locale']")
//    WebElement langIconOnDropDown;


    public void openURL(String urlHere) {
        driver.get(urlHere);
    }

    public void clickOnLanguageDropdown() {
        langDropdown.click();


    }


//    @DataProvider(name = "languageList")
//    public Object[][] getData() throws Exception {
//        Object[][] data = new Object[3][1];
//        data[0][0] = "RU";
//        data[1][0] = "UA";
//        data[2][0] = "ES";
//
//        return data;
//    }
//
//
//
//    public void chooseLanguage(String lang) throws Exception {
//        String languageVar = "languageList";
//        driver.findElement(By.xpath(""));
//
//
//    }
    public void findValueOfThisCookieByName (String cookieName) throws Exception{
        driver.manage().getCookieNamed(cookieName).getValue();
    }
}
